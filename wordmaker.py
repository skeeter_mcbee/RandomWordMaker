import random

'''
    Try to make a word using random letters. A word is considered done when a space is found.
'''
def makeAWord(letters):
    letter = ''         # The chosen letter
    word = ''           # The whole word
    lastLetter = ''     # The last letter used
    while lastLetter != ' ':    # Stop when a space is found
        letter = random.choice(letters)     # Get a random letter from the list
        word += str(letter) # Add the letter to the word
        lastLetter = letter # Set the last letter to the current letter
    return word.strip()     # Remove the trailing space from the word and return it.

'''
    Strip the newline character from the dictionary words.
'''
def cleanup(words):
    for i in range(len(words)):
        words[i] = words[i].rstrip('\n').lower()
    return words



letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
		   'r','s', 't','u','v','w','x','y','z',' ']
madeWords = open('made_words.txt', 'w+')        # File to store the words we made (for the hell of it.  May be removed
                                                # in future version)
madeRealWords = open('real_words.txt', 'w+')    # File to store real words for review
dictionary = open("words_alpha.txt")            # File that holds all dictionary words
words = dictionary.readlines()                  # Make a list of all the dictionary words
words = cleanup(words)                          # Clean the dictionary list.
i = 0               # Iterator
limit = 100000  # Limit of words
while i < limit:
    word = makeAWord(letters)
    madeWords.write(word + '\n')
    print(str(i) + " of " + str(limit))     # This is here to give some form of output to ensure the program is still
                                            # running
    i += 1
    if word in words:
        madeRealWords.write(word + '\n')

# Close the files when done.
madeWords.close()
madeRealWords.close()
dictionary.close()