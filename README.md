# RandomWordMaker

Makes a bunch of random words, checks them against a dictionary.

Runs on Python 2.

Master branch is considered stable. Do not push to the master branch. A testing
branch is available.